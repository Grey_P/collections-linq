﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionAndLinq.Models
{
    public class TeamModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
    }
}
