﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollectionAndLinq.Models;

namespace CollectionAndLinq.Models.ResponseModel
{
    public class PeopleWithTasksModel
    {
        public UserModel User { get; set; }
        public List<TaskModel> Tasks { get; set; }
    }
}
