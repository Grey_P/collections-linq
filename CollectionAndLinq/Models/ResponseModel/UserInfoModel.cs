﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionAndLinq.Models.ResponseModel
{
    public class UserInfoModel
    {
        // I thought about LastProjectID and LongestTaskId...
        public UserModel User { get; set; }
        public ProjectModel LastProject { get; set; }
        public int TaskNumberForLastProject { get; set; }
        public int UndoneTasksNumber { get; set; }
        public TaskModel LongestTask { get; set; }
    }
}
