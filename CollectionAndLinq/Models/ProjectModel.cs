﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionAndLinq.Models
{
    public class ProjectModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("authorId")]
        public int AuthorId { get; set; }

        [JsonProperty("teamId")]
        public int TeamId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        public List<TaskModel> Tasks = new List<TaskModel>();
        public UserModel Author { get; set; }
        public TeamModel Team { get; set; }
    }
}
