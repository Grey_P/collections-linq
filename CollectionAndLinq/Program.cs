﻿using System;
using CollectionAndLinq.Services;
using CollectionAndLinq.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using CollectionAndLinq.Models.ResponseModel;

namespace CollectionAndLinq
{
    class Program
    {
        static async Task Main(string[] args)
        {
            HttpService ser = new HttpService();
            List<TaskModel> tasks = (List<TaskModel>)await ser.GetTasksFromAPI();
            List<ProjectModel> projects = (List<ProjectModel>)await ser.GetProjectsFromAPI();
            List<TeamModel> teams = (List<TeamModel>)await ser.GetTeamsFromAPI();
            List<UserModel> users = (List<UserModel>)await ser.GetUsersFromAPI();

            DataReqestsService dtReqSer = new DataReqestsService(projects, tasks, teams, users);

            // --------< Task1 >--------------
            Console.WriteLine("Task1:");
            var task1 = dtReqSer.GetNumOfTaskByAuthorID(31);
            foreach (var el in task1)
            {
                Console.WriteLine("IdProject: " + el.Key + " TuskNum: " + el.Value);
            }
            //----------------------------------


            //----------< Task2 >--------------
            Console.WriteLine("\nTask2:");
            var task2 = dtReqSer.GetListOfTaskForPerformer(43);
            Console.WriteLine("Elemenst in dict: " + task2.Count);
            foreach (var el in task2)
            {
                Console.WriteLine("ProjectId: " + el.ProjectId + ". Task name: " + el.Name);
            }
            //---------------------------------


            //-----------< Task3 >------------
            Console.WriteLine("\nTask3");
            var task3 = dtReqSer.GetFinishedTaskByUserID(110);
            foreach (var el in task3)
            {
                Console.WriteLine("Id Task: " + el.Id + ". Task name: " + el.Name);
            }
            //---------------------------------


            //------------< Task4 >-----------
            Console.WriteLine("\nTask4");
            var task4 = dtReqSer.GetTeamPeopleOlderThen10();
            foreach (var el in task4)
            {
                Console.WriteLine("Id Team: " + el.Id + ". Team name: " + el.Name);
            }


            //------------< Task5 >-----------
            Console.WriteLine("\nTask5");
            var task5 = dtReqSer.GetPeopleWithTask();
            
            foreach (var el in task5)
            {
                Console.WriteLine("User ID: " + el.User.FirstName + ". Tusk number: " + el.Tasks.Count);
            }
            //---------------------------------


            //-----------< Task6 >-----------
             Console.WriteLine("\nTask6");
             var task6 = dtReqSer.GetUserInfo(74);
             Console.WriteLine("Name:" + task6.User.FirstName + ". Task num for last project:" + task6.TaskNumberForLastProject);


            //-----------< Task7 >------------
            Console.WriteLine("\nTask7");
            var task7 = dtReqSer.GetProjectInfo();
            foreach (var el in task7)
            {
                Console.WriteLine("ProjID: " + el.Project.Id + ". Users number: " + el.UserNumber);
            }
            //--------------------------------


            Console.WriteLine("The end :)");
            Console.ReadKey();
        }
    }
}
