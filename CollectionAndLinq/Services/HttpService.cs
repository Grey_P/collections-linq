﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CollectionAndLinq.Models;
using Newtonsoft.Json;

namespace CollectionAndLinq.Services
{
    public class HttpService
    {
        private HttpClient client;
        private string url = "https://bsa21.azurewebsites.net/api/"; 
        
        public HttpService()
        {
            client = new HttpClient();
        }

        public async Task<IEnumerable<TaskModel>> GetTasksFromAPI()
        {
            HttpResponseMessage response = await client.GetAsync(url + "Tasks");
            return JsonConvert.DeserializeObject<IEnumerable<TaskModel>>(response.Content.ReadAsStringAsync().Result);
        }

        public async Task<IEnumerable<ProjectModel>> GetProjectsFromAPI()
        {
            HttpResponseMessage response = await client.GetAsync(url + "Projects");
            return JsonConvert.DeserializeObject<IEnumerable<ProjectModel>>(response.Content.ReadAsStringAsync().Result);
        }

        public async Task<IEnumerable<TeamModel>> GetTeamsFromAPI()
        {
            HttpResponseMessage response = await client.GetAsync(url + "Teams");
            return JsonConvert.DeserializeObject<IEnumerable<TeamModel>>(response.Content.ReadAsStringAsync().Result);
        }

        public async Task<IEnumerable<UserModel>> GetUsersFromAPI()
        {
            HttpResponseMessage response = await client.GetAsync(url + "Users");
            return JsonConvert.DeserializeObject<IEnumerable<UserModel>>(response.Content.ReadAsStringAsync().Result);
        }
    }
}
