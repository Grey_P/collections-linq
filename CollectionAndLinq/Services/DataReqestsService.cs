﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CollectionAndLinq.Models;
using CollectionAndLinq.Models.ResponseModel;

namespace CollectionAndLinq.Services
{
    public class DataReqestsService
    {
        List<ProjectModel> Projects;
        List<TaskModel> Tasks;
        List<TeamModel> Teams;
        List<UserModel> Users;
        public DataReqestsService(IEnumerable<ProjectModel> projects,
                                  IEnumerable<TaskModel> tasks,
                                  IEnumerable<TeamModel> teams,
                                  IEnumerable<UserModel> users)
        {
            this.Projects = (List<ProjectModel>)projects;
            this.Tasks = (List<TaskModel>)tasks;
            this.Teams = (List<TeamModel>)teams;
            this.Users = (List<UserModel>)users;

            // ---------------------------
            // Join performer to every task
            // 
            Tasks = Tasks.Join(Users,
                               t => t.PerformerId,
                               u => u.Id,
                               (t, u) =>
                               {
                                   t.Performer = u;
                                   return t;
                               }).ToList();

            //----------------------------
            // Join Tasks to every Project
            Projects = Projects.GroupJoin(Tasks,
                                    p => p.Id,
                                    t => t.ProjectId,
                                    (p, t) => 
                                    {
                                       p.Tasks = p.Tasks.Union(t.Select(b => b).ToList()).ToList();
                                      //  p.Tasks = (List<TaskModel>)t;
                                        return p;
                                    }).ToList();

            //----------------------------------------
            // Join Author to every Project
            Projects = Projects.Join(Users,
                                    p => p.AuthorId,
                                    u => u.Id,
                                    (p, t) =>
                                    {
                                        p.Author = t;
                                        return p;
                                    }).ToList();

            //----------------------------------------
            // Join Team to every Project
            Projects = Projects.Join(Teams,
                                    p => p.TeamId,
                                    t => t.Id,
                                    (p, t) =>
                                    {
                                        p.Team = t;
                                        return p;
                                    }).ToList();

            // <--- At the moment we have structure like we were supposed
            // _Project
            //  |_Tasks
            //      |___Performer
            //  |__Author
            //  |__Team
        }

        //--------------------------------------
        // Task 1
        // Отримати кількість тасків у проекті конкретного користувача 
        // (по id) (словник, де key буде проект, а value кількість тасків).
        public IDictionary<int, int> GetNumOfTaskByAuthorID(int id)
        {
            return Projects.Where(p => p.AuthorId == id)
                .Select(p => new {key = p.Id,value = p.Tasks.Count }).
                Distinct().ToDictionary(result => result.key, result => result.value);
        }

        //----------------------------------------
        // Task2
        // Отримати список тасків, призначених для конкретного користувача (по id), 
        // де name таска <45 символів (колекція з тасків).
        public ICollection<TaskModel> GetListOfTaskForPerformer(int id)
        {
            return Tasks.Where(t => t.PerformerId == id).Where(t => t.Name.Length < 45).Select(t => t).ToList();
        }

        //---------------------------------------
        // Task 3
        // Отримати список (id, name) з колекції тасків, які виконані (finished)
        // в поточному (2021) році для конкретного користувача (по id).
        public IEnumerable<IdNameOfTaskModel> GetFinishedTaskByUserID(int Authorid)
        {
            return Tasks.Where(t => t.PerformerId == Authorid)
                .Where(t => t.FinishedAt != null && t.FinishedAt.Value.Year == DateTime.Now.Year)
                .Select(t => new IdNameOfTaskModel(){ Id = t.Id, Name = t.Name }).ToList();
        }

        //---------------------------------------
        // Task 4
        // Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років,
        // відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
        public IEnumerable<TeamModel> GetTeamPeopleOlderThen10()
        {
            return Users.OrderByDescending(u => u.RegisteredAt).
                          GroupBy(u => u.TeamId).
                          Where(u => u.All(a => DateTime.Now.Year - a.BirthDay.Year >= 10)).
                          Join(Teams,
                          u => u.Key,
                          t => t.Id,
                          (u, t) => { return t; }).ToList();
        }

        //--------------------------------------
        // Task 5
        // Отримати список користувачів за алфавітом first_name (по зростанню) 
        //з відсортованими tasks по довжині name (за спаданням).
        public IEnumerable<PeopleWithTasksModel> GetPeopleWithTask()
        {
            return Tasks.OrderByDescending(t => t.Name.Length).
                          GroupBy(t => t.PerformerId).
                          Join(Users,
                          t => t.Key,
                          u => u.Id,
                          (t, u) => new PeopleWithTasksModel()
                          { User = u, Tasks = t.ToList() }).
                          OrderBy(obj => obj.User.FirstName).
                          ToList();
        }   //? In this one, if user hasn`t any task, he will not be in result 


        //-----------------------------------------
        // Task 6
        // Отримати наступну структуру (передати Id користувача в параметри):
        // - User - Останній проект користувача (за датою створення) - Загальна кількість тасків під останнім проектом -
        // Загальна кількість незавершених або скасованих тасків для користувача - Найтриваліший таск користувача за датою (найраніше створений - найпізніше закінчений)
        public UserInfoModel GetUserInfo(int id)
        {
            return Users.Where(u => u.Id == id).Join(Projects,
                             u => u.TeamId,
                             p => p.TeamId,
                             (u, p) => new UserInfoModel()
                             {
                                 User = u,
                                 LastProject = p,
                                 TaskNumberForLastProject = p.Tasks.Count,
                                 UndoneTasksNumber = Tasks.Where(el => (el.PerformerId == u.Id) && (el.FinishedAt == null)).Count(),
                                 LongestTask = Tasks.Where(el => (el.PerformerId == u.Id) && (el.FinishedAt != null)).
                                  OrderByDescending(el => el.FinishedAt - el.CreatedAt).ToList().FirstOrDefault()
                             }).ToList().OrderByDescending(el => el.LastProject.CreatedAt).Take(1).ToList().FirstOrDefault();
        }


        //-----------------------------------------------
        // Task 7
        // Отримати таку структуру: Проект - Найдовший таск проекту (за описом) -
        // Найкоротший таск проекту (по імені) - Загальна кількість користувачів в команді проекту,
        // де або опис проекту >20 символів, або кількість тасків <3
        public IEnumerable<ProjectInfoModel> GetProjectInfo()
        {
            return Projects.Select(p => new ProjectInfoModel()
            {
                Project = p,
                LongestTask = p.Tasks.OrderByDescending(el => el.Description.Length).FirstOrDefault(),
                ShorterTask = p.Tasks.OrderBy(el => el.Name.Length).FirstOrDefault(),
                UserNumber = Users.Where(user => user.TeamId == p.TeamId
                && (p.Description.Length > 20
                || p.Tasks.Count < 3)).Count()
            }).ToList();
        }
    }
}
